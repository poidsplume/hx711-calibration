# Shiny app for calibration of a HX711 

These scripts are the source code for running a shiny app to calibrate 
a HX711 amplifier (commonly used to read load cells). You can acces the 
app a the following address: 

[https://poidsplume.shinyapps.io/shinyapp/](https://poidsplume.shinyapps.io/shinyapp/)
